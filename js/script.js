
 function filterBy (array, argument) {
    const filterArray = array.filter(function(item) {
        if (typeof item !== argument){
           return true;
        }
    });
    return filterArray;
 }

// Test
// console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));